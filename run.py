
def f_and(a, b):
    return (a == 1) and (b == 1)


def f_or(a, b):
    return (a == 1) or (b == 1)


def f_xor(a, b):
    return a ^ b


def f_not(a):
    return not a


def f_a(y_0, y_1, y_2, y_3):
    ans = f_xor(
        f_xor(f_or(y_0, y_1), f_and(y_0, y_3)),
        f_and(y_2, f_or(f_xor(y_0, y_1), y_3))
    )
    return ans


def f_b(y_0, y_1, y_2, y_3):
    ans = f_xor(f_or(f_and(y_0, y_1), y_2),
                f_and(f_xor(y_0, y_1), f_or(y_2, y_3))
                )
    return ans


def f_c(y_0, y_1, y_2, y_3, y_4):
    ans = f_xor(
        f_or(y_0, f_and(f_or(y_1, y_4), f_xor(y_3, y_4))),
        f_and(f_xor(y_0, f_and(y_1, y_3)),
              f_or(f_xor(y_2, y_3), f_and(y_1, y_4))
              )
    )
    return ans


def f_full(bit48_arr):
    return bool(
        f_c(f_a(bit48_arr[9], bit48_arr[11], bit48_arr[13], bit48_arr[15]),
            f_b(bit48_arr[17], bit48_arr[19], bit48_arr[21], bit48_arr[23]),
            f_b(bit48_arr[25], bit48_arr[27], bit48_arr[29], bit48_arr[31]),
            f_a(bit48_arr[33], bit48_arr[35], bit48_arr[37], bit48_arr[39]),
            f_b(bit48_arr[41], bit48_arr[43], bit48_arr[45], bit48_arr[47])))


def int_to_bytearr(int_num, length):
    arr = [int(x) for x in list(bin(int_num)[2:].zfill(length))]
    return arr


def bytearr_to_int(byte_arr):
    return int(''.join([str(int(x)) for x in byte_arr]), 2)


def L_1(bit16_arr):
    return bool(bit16_arr[0] ^ bit16_arr[2] ^ bit16_arr[3] ^ bit16_arr[5])


def suc(bit32_arr):
    new_arr = [x for x in bit32_arr[1:]] + [L_1(bit32_arr[16:])]
    return new_arr


def little_endian_32bit(arr):
    chunks, chunk_size = len(arr), 8
    tmp_arr = [arr[i:i + chunk_size] for i in range(0, chunks, chunk_size)][
              ::-1]
    res_arr = []
    for x in tmp_arr:
        res_arr += x
    return res_arr


class LFSR_reader:
    l_byte_arr = []

    def __init__(self, key_int, N_t, UID, N_r):
        self.l_byte_arr = int_to_bytearr(key_int, 48)[::-1]

        print('1) LFSR: {:10x}'.format(bytearr_to_int(self.l_byte_arr[::-1])))


        N_t_bits = little_endian_32bit(int_to_bytearr(N_t, 32)[::-1])
        UID_bits = little_endian_32bit(int_to_bytearr(UID, 32)[::-1])

        xor_N_uid = [f_xor(a, b) for a, b in zip(N_t_bits, UID_bits)]
        #xor_N_uid = little_endian_32bit(int_to_bytearr(N_t ^ UID, 32))[::-1]

        print('   UID xor Nt: {:10x}'.format(bytearr_to_int(xor_N_uid)))

        # word(Nt ^ UID)

        for x in range(len(xor_N_uid)):
            self.vect_init_xor(xor_N_uid[x])

        print('2) LSFR UID xor Nt: {:10x}'.format(
            bytearr_to_int(self.l_byte_arr[::-1])))


        N_r_bits = little_endian_32bit(int_to_bytearr(N_r, 32)[::-1])
        ks1 = []
        # ks1 = word(Nr)
        for x in range(len(N_r_bits)):
            k = f_full(self.l_byte_arr)
            ks1.append(k)
            self.vect_init_xor(N_r_bits[x])
        print('3) LSFR xor Nr: {:10x}'.format(
            bytearr_to_int(self.l_byte_arr[::-1])))

        print('   Ks1: {}'.format(hex(bytearr_to_int(little_endian_32bit(ks1[::-1])))))

        N_r_bits_enc = [f_xor(a, b) for a, b in zip(ks1, N_r_bits)]

        print('   {{Nr}}: {:08x}'.format(bytearr_to_int(little_endian_32bit(N_r_bits_enc)[::-1])))
        # Create suc_64(Nt)

        suc64_Nt = N_t_bits
        for x in range(64):
            suc64_Nt = suc(suc64_Nt)

        print('4) suc64(Nt)==Nt\'==Ar: {:08x}'.format(
            bytearr_to_int(little_endian_32bit(suc64_Nt[::-1]))))

        # add N_r to stream
        # N_r_bits
        ks2 = self.get_bitstream(32)

        print('5) Ks2: {}'.format(hex(bytearr_to_int(little_endian_32bit(ks2[::-1])))))
        A_r_enc = [f_xor(a, b) for a, b in zip(ks2, suc64_Nt)]
        print('   {{Ar}}: {}'.format(hex(bytearr_to_int(little_endian_32bit(A_r_enc[::-1])))))

        #TODO: input {At}


        A_t = 0xa79200d3
        print('6) Input A_t: {:08x}'.format(A_t))
        A_t_bits = little_endian_32bit(int_to_bytearr(A_t, 32)[::-1])
        ks3 = self.get_bitstream(32)
        print('   Ks3: {}'.format(hex(bytearr_to_int(little_endian_32bit(ks3[::-1])))))
        suc96_Nt = [f_xor(a, b) for a, b in zip(ks3, A_t_bits)]
        print('   At == Nt\'\' == suc96(Nt): {}'.format(hex(bytearr_to_int(little_endian_32bit(suc96_Nt[::-1])))))
        suc96_Nt_rdr = N_t_bits
        for x in range(96):
            suc96_Nt_rdr = suc(suc96_Nt_rdr)
        print('   My suc96(Nt) == {}'.format(hex(bytearr_to_int(little_endian_32bit(suc96_Nt_rdr[::-1])))))


        enc_msg = 0xa386a0a9
        print("7) Input Enc_message read: {:08x}".format(enc_msg))
        enc_msg_bits = little_endian_32bit(int_to_bytearr(enc_msg, 32)[::-1])
        ks4 = self.get_bitstream(32)
        decr_msg = [f_xor(a, b) for a, b in zip(ks4, enc_msg_bits)]
        print('   Decrypted message: {}'.format(
            hex(bytearr_to_int(little_endian_32bit(decr_msg[::-1])))))

        return

    def shift_L(self):
        xor_vals_num = [0, 5, 9, 10, 12, 14, 15,
                        17, 19, 24, 25, 27, 29,
                        35, 39, 41, 42, 43]
        ans = 0
        for num in xor_vals_num:
            ans = f_xor(ans, self.l_byte_arr[num])

        self.l_byte_arr.append(ans)
        return ans

    '''
    def back_R(self, index):
        xor_vals_num = [4, 8, 9, 11, 13, 14, 16, 18, 23, 24, 26, 28, 34, 38, 40,
                        42, 47]
        ans = 0
        for num in xor_vals_num:
            ans = f_xor(ans, self.l_byte_arr[index + num])
        self.l_byte_arr[index] = ans
        return ans
    '''

    def vect_init_xor(self, to_enc_bit):
        self.shift_L()
        #print('{} ^ enc {} == {}'.format(int(self.l_byte_arr[-1]),to_enc_bit,self.l_byte_arr[-1] ^ to_enc_bit))
        self.l_byte_arr[-1] = self.l_byte_arr[-1] ^ to_enc_bit
        del self.l_byte_arr[0]

    def get_bitstream(self,length):
        ans = []
        for x in range(length):
            k = f_full(self.l_byte_arr)
            ans.append(k)
            self.shift_L()
            del self.l_byte_arr[0]
        return ans



def main():
    UID = 0xdcde7000
    N_t = 0x72569afc
    K = 0xa5a4a3a2a1a0
    N_r = 0x55414992

    print('UID: {:08x}'.format(UID))
    print('Nt: {:08x}'.format(N_t))
    print('Block key: {:10x}'.format(K))
    print('Nr: {:08x}'.format(N_r))
    res = LFSR_reader(K, N_t, UID, N_r)
    print(len(res.l_byte_arr))
    input()


main()
